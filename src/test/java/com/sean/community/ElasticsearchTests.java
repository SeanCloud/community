package com.sean.community;

import com.alibaba.fastjson.JSONObject;
import com.sean.community.dao.DiscussPostMapper;
import com.sean.community.dao.elasticsearch.DiscussPostRepository;
import com.sean.community.entity.DiscussPost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ElasticsearchTests {
    @Autowired
    private DiscussPostMapper discussPostMapper;
    @Autowired
    private DiscussPostRepository discussPostRepository;
    @Autowired
    @Qualifier("elasticsearchClient")
    private RestHighLevelClient restHighLevelClient;

    @Test
    public void testInsert(){
        // 添加单个数据
        discussPostRepository.save(discussPostMapper.selectDiscussPostById(241));
        discussPostRepository.save(discussPostMapper.selectDiscussPostById(242));
        discussPostRepository.save(discussPostMapper.selectDiscussPostById(243));
    }

    @Test
    public void testInsertList(){
        // 添加多行数据
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPosts(101, 0, 100, 0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPosts(102, 0, 100, 0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPosts(103, 0, 100, 0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPosts(111, 0, 100, 0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPosts(112, 0, 100, 0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPosts(131, 0, 100, 0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPosts(132, 0, 100, 0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPosts(133, 0, 100, 0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPosts(134, 0, 100, 0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPosts(149, 0, 100, 0));
        discussPostRepository.saveAll(discussPostMapper.selectDiscussPosts(153, 0, 100, 0));
    }

    @Test
    public void testUpdate(){
        DiscussPost discussPost = discussPostMapper.selectDiscussPostById(231);
        discussPost.setContent("新人疯狂灌水");
        discussPostRepository.save(discussPost);
    }

    @Test
    public void testDelete(){
        // discussPostRepository.deleteById(231);
        discussPostRepository.deleteAll();
    }

//    7.x 不可用
//    @Test
//    public void testSearch(){
//        // 1. 构造搜索条件
//        // 排序，分页，高亮
//        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
//                .withQuery(QueryBuilders.multiMatchQuery("互联网寒冬", "title", "content"))
//                .withSorts(SortBuilders.fieldSort("type").order(SortOrder.DESC))
//                .withSorts(SortBuilders.fieldSort("score").order(SortOrder.DESC))
//                .withSorts(SortBuilders.fieldSort("creatTime").order(SortOrder.DESC))
//                .withPageable(PageRequest.of(0, 10))
//                .withHighlightFields(
//                        new HighlightBuilder.Field("title").preTags("<em>").postTags("</em>"),
//                        new HighlightBuilder.Field("content").preTags("<em>").postTags("</em>")
//                ).build();
//        discussPostRepository.search(searchQuery);
//    }

    // 无高亮查询
    @Test
    public void test7_xSearch() throws IOException {
        //构建查询请求对象, 指定查询的索引名称
        SearchRequest searchRequest = new SearchRequest("discusspost");
        //创建 查询条件构建器
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(QueryBuilders.multiMatchQuery("互联网寒冬", "title", "content"))
                .sort(SortBuilders.fieldSort("type").order(SortOrder.DESC))
                .sort(SortBuilders.fieldSort("score").order(SortOrder.DESC))
                .sort(SortBuilders.fieldSort("createTime").order(SortOrder.DESC))
                .from(0)
                .size(10);
        searchRequest.source(searchSourceBuilder);
        SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        // 获取结果
        List<DiscussPost> list = new ArrayList<>();
        SearchHit[] hits = response.getHits().getHits();
        for(SearchHit hit : hits){
            list.add(JSONObject.parseObject(hit.getSourceAsString(), DiscussPost.class));
        }
        System.out.println(list.size());
        for (DiscussPost discussPost : list) {
            System.out.println(discussPost);
        }
    }

    // 高亮查询
    @Test
    public void testHighLightSearch() throws IOException {
        //构建查询请求对象, 指定查询的索引名称
        SearchRequest searchRequest = new SearchRequest("discusspost");
        //创建 查询条件构建器
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .query(QueryBuilders.multiMatchQuery("互联网寒冬", "title", "content"))  // 查询字段匹配
                .sort(SortBuilders.fieldSort("type").order(SortOrder.DESC))         // 排序
                .sort(SortBuilders.fieldSort("score").order(SortOrder.DESC))
                .sort(SortBuilders.fieldSort("createTime").order(SortOrder.DESC))
                .from(0)                            // 分页
                .size(10)
                .highlighter(new HighlightBuilder() // 高亮
                        .field("title")
                        .field("content")
                        .preTags("<em>")
                        .postTags("</em>")
                        .requireFieldMatch(false)
                );
        // 设置搜索请求的源
        searchRequest.source(searchSourceBuilder);
        // 调用 es 进行 查询
        SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        // 处理结果
        List<DiscussPost> list = new ArrayList<>();
        SearchHit[] hits = response.getHits().getHits();
        for(SearchHit hit : hits){
            DiscussPost discussPost = JSONObject.parseObject(hit.getSourceAsString(), DiscussPost.class);
            // 高亮替换
            HighlightField titleField = hit.getHighlightFields().get("title");
            if (titleField != null) {
                // 只将第一个匹配词设置为高亮，例如，互联网寒冬互联网寒冬，只有第一个互联网寒冬为高亮
                discussPost.setTitle(titleField.getFragments()[0].toString());
            }
            HighlightField contentField = hit.getHighlightFields().get("content");
            if (contentField != null) {
                discussPost.setContent(contentField.getFragments()[0].toString());
            }
            list.add(discussPost);
        }
        System.out.println(list.size());
        for (DiscussPost discussPost : list) {
            System.out.println(discussPost);
        }
    }
}
