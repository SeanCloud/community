package com.sean.community;

import com.sean.community.dao.*;
import com.sean.community.entity.*;
import org.checkerframework.checker.units.qual.A;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
@RunWith(SpringRunner.class)
@SpringBootTest
public class MapperTests {

    @Autowired
    private DiscussPostMapper postMapper;
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private MessageMapper messageMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private LoginTicketMapper loginTicketMapper;

    @Test
    public void testSelectPost(){
        List<DiscussPost> discussPosts = postMapper.selectDiscussPosts(0, 0, 10, 0);
        for (DiscussPost post : discussPosts) {
            System.out.println(post);
        }

        Integer rows = postMapper.selectDiscussPostRows(0);
        System.out.println(rows);
    }

    @Test
    public void testSelectUser(){
        System.out.println(userMapper.selectById(149));
        System.out.println(userMapper.selectByName("niuke"));
        System.out.println(userMapper.selectByEmail("nowcoder149@sina.com"));
    }

    @Test
    public void testInsertUser(){
        User user = new User();
        user.setUsername("test");
        user.setPassword("123456");
        user.setSalt("0000");
        user.setEmail("test@qq.com");
        user.setHeaderUrl("http://www.nowcoder.com/101.png");  // 0 - 1000
        user.setCreateTime(new Date());
        System.out.println(userMapper.insertUser(user));
        System.out.println(user.getId());
    }

    @Test
    public void testUpdate(){
        System.out.println(userMapper.updateStatus(150, 1));
        System.out.println(userMapper.updatePassword(150, "1224521413"));
        System.out.println(userMapper.updateHeader(150, "http://www.nowcoder.com/102.png"));
    }

    @Test
    public void testLoginTicketMapper(){
        LoginTicket loginTicket = new LoginTicket();
        loginTicket.setUserId(111);
        loginTicket.setTicket("abc");
        loginTicket.setStatus(0);
        loginTicket.setExpired(new Date(System.currentTimeMillis() + 1000 * 60 * 10));

//         loginTicketMapper.insertLoginTicket(loginTicket);
//
        LoginTicket loginTicket1 = loginTicketMapper.selectByTicket("abc");
        System.out.println(loginTicket1);

        System.out.println("==================update====================");
        loginTicketMapper.updateStatus("abc", 1);
        loginTicket1 = loginTicketMapper.selectByTicket("abc");
        System.out.println(loginTicket1);

    }


    @Test
    public void testCommentMapper(){
        List<Comment> comments = commentMapper.selectCommentsByEntity(1, 228, 0, 10);
        System.out.println(comments);
        System.out.println(commentMapper.selectCountByEntity(1, 228));
    }


    @Test
    public void testMessageMapper(){
        List<Message> messages = messageMapper.selectConversations(111, 0, 20);
        for(Message message : messages){
            System.out.println(message);
        }
        System.out.println(messageMapper.selectConversationCount(111));

        List<Message> messages1 = messageMapper.selectLetters("111_112", 0, 10);
        for(Message message : messages1){
            System.out.println(message);
        }
        System.out.println(messageMapper.selectLetterCount("111_112"));

        System.out.println(messageMapper.selectLetterUnreadCount(111, null));
        System.out.println(messageMapper.selectLetterUnreadCount(131, "111_131"));

    }

    @Test
    public void testCommentMapperSelectByUser(){
        final List<Comment> comments = commentMapper.selectDiscussPostCommentByUser(153, 0, 10);
        for (Comment comment : comments) {
            System.out.println(comment);
        }
    }

    @Test
    public void testCommentMapperSelectCountByUser(){
        System.out.println(
                commentMapper.selectDiscussPostCommentCountByUser(153)
        );
    }
}
