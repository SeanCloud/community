package com.sean.community;

import com.sean.community.util.MailClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailTests {
    MailClient mailClient;
    TemplateEngine templateEngine;

    @Autowired
    public void setMailClient(MailClient mailClient) {
        this.mailClient = mailClient;
    }

    @Autowired
    public void setTemplateEngine(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    @Test
    public void testTextMail(){
        mailClient.sendMail("402399664@qq.com", "TEST", "Hello World");
    }

    @Test
    public void testHtmlMail(){
        Context context = new Context();
        context.setVariable("username", "Sean");    // 模板引擎参数
        String content = templateEngine.process("/mail/demo", context); // 调用模板引擎生成静态 HTML
        System.out.println(content);

        mailClient.sendMail("402399664@qq.com", "HTML", content);
    }
}
