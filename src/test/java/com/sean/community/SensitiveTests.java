package com.sean.community;


import com.sean.community.util.SensitiveFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SensitiveTests {
    @Autowired
    SensitiveFilter sensitiveFilter;

    @Test
    public void testFilter(){
        String text = "不能，赌，博，赌钱博，草一种植物，草你---妈，不能嫖";
        String filter_string = sensitiveFilter.filter(text);
        System.out.println(filter_string);
    }
}
