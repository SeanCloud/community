package com.sean.community;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ThreadPoolTests {
    private static final Logger logger = LoggerFactory.getLogger(ThreadPoolTests.class);

    // JDK 普通线程池 (初始哈 5 个线程)
    private ExecutorService executorService = Executors.newFixedThreadPool(5);
    // JDK 定时任务线程池
    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
    // Spring 普通线程池
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    // Spring 定时任务线程池
    @Autowired
    private ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    private TestExecutor executor;

    private void sleep(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // main 启动线程： main 会等待线程结束
    // junit 启动线程：线程是并发

    @Test
    public void testExecutorService(){
        Runnable task = new Runnable(){
            @Override
            public void run() {
                logger.debug("hello ExecutorService");
            }
        };
        for(int i = 0; i < 10; i++){
            executorService.submit(task);   // 线程池会分配一个线程来执行任务（线程体）
        }
        this.sleep(10000);
    }

    @Test
    public void testScheduledExecutorService(){
        Runnable task = new Runnable() {
            @Override
            public void run() {
                logger.debug("hello ScheduledExecutorService");
            }
        };
        // 任务，推迟执行时间，时间间隔，单位
        scheduledExecutorService.scheduleAtFixedRate(task, 10000, 1000, TimeUnit.MILLISECONDS);
        this.sleep(30000);
    }

    @Test
    public void testThreadPoolTaskExecutor(){
        Runnable task = new Runnable(){
            @Override
            public void run() {
                logger.debug("hello ThreadPoolTaskExecutor");
            }
        };
        for(int i = 0; i < 10; i++){
            taskExecutor.submit(task);   // 线程池会分配一个线程来执行任务（线程体）
        }
        this.sleep(10000);
    }
    // Spring 定时任务线程池
    @Test
    public void testThreadPoolTaskScheduler(){
        Runnable task = new Runnable() {
            @Override
            public void run() {
                logger.debug("hello ThreadPoolTaskScheduler");
            }
        };
        // 任务，开始执行时间，时间间隔
        Date startTime = new Date(System.currentTimeMillis() + 10000);
        taskScheduler.scheduleAtFixedRate(task, startTime, 1000);
        this.sleep(30000);
    }


    // Spring 普通线程池简化使用方式
    @Test
    public void testExecutor(){
        for (int i = 0; i < 10; i++) {
            executor.executor1();
        }
        this.sleep(10000);
    }

    // Spring 定时线程池简化使用
    @Test
    public void testScheduledExecutor(){
        this.sleep(30000);
    }
}

@Component
class TestExecutor{
    private static final Logger logger = LoggerFactory.getLogger(ThreadPoolTests.class);
    // 该方法作为线程体
    // 让该方法在多线程环境下，被异步调用
    @Async
    public void executor1(){
        logger.debug("executor1");
    }
    // 定时执行
//    @Scheduled(initialDelay = 10000, fixedRate = 1000)
//    public void executor2(){
//        logger.debug("executor2");
//    }
}
