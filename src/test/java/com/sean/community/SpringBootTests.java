package com.sean.community;

import com.sean.community.entity.DiscussPost;
import com.sean.community.service.DiscussPostService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootTests{

    @Autowired
    private DiscussPostService service;

    private DiscussPost discussPost;

    @BeforeClass
    public static void beforeClass(){
        // 类加载之前执行
        System.out.println("before class");
    }

    @Before
    public void before(){
        // 测试方法前执行
        System.out.println("before");
        // 初始化测试数据
        discussPost = new DiscussPost();
        discussPost.setUserId(111);
        discussPost.setTitle("Test Title");
        discussPost.setContent("Test Content");
        discussPost.setCreateTime(new Date());
        service.addDiscussPost(discussPost);
    }

    @Test
    public void test1(){
        System.out.println("test1");
    }

    @Test
    public void test2(){
        System.out.println("test2");
    }

    @Test
    public void testFindById(){
        DiscussPost post = service.findDiscussPostById(discussPost.getId());
        Assert.assertNotNull(post);
        Assert.assertEquals(discussPost.getTitle(), post.getTitle());
        Assert.assertEquals(discussPost.getContent(), post.getContent());
    }

    @Test
    public void testUpdateScore(){
        int rows = service.updateScoreById(discussPost.getId(), 2000);
        Assert.assertEquals(1, rows);
        DiscussPost post = service.findDiscussPostById(discussPost.getId());
        Assert.assertEquals(2000.00, post.getScore(), 2);
    }

    @After
    public void after(){
        // 测试方法后执行
        System.out.println("after");
        // 删除测试数据
        service.updateStatusById(discussPost.getId(), 2);
    }

    @AfterClass
    public static void afterClass(){
        // 类销毁后执行
        System.out.println("after class");
    }
}
