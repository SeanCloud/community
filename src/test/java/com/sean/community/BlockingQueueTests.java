package com.sean.community;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 阻塞队列案例测试
 */
public class BlockingQueueTests {
    public static void main(String[] args) {
        BlockingQueue<Integer> blockingQueue = new ArrayBlockingQueue<>(10);
        // 生产者
        new Thread(new Producer(blockingQueue)).start();
        // 消费者
        new Thread(new Consumer(blockingQueue)).start();
        new Thread(new Consumer(blockingQueue)).start();
        new Thread(new Consumer(blockingQueue)).start();
    }
}


/**
 * 生产者
 */
class Producer implements Runnable{
    BlockingQueue<Integer> blockingQueue;

    public Producer(BlockingQueue<Integer> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try{
            for(int i = 0; i < 100; i ++){
                Thread.sleep(20);   // 每 20ms 生产一个 int
                blockingQueue.put(i);
                System.out.println(Thread.currentThread().getName()
                        + "生产： " + this.blockingQueue.size());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

class Consumer implements Runnable{
    BlockingQueue<Integer> blockingQueue;

    public Consumer(BlockingQueue<Integer> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try{
           while(true){
                Thread.sleep(new Random().nextInt(1000));   // 随机时间消费一个 int
                blockingQueue.take();
                System.out.println(Thread.currentThread().getName()
                        + "消费： " + this.blockingQueue.size());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
