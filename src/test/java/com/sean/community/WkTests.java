package com.sean.community;

import java.io.IOException;

public class WkTests {
    public static void main(String[] args) {
        String cmd = "D:/wkhtmltoX/wkhtmltopdf/bin/wkhtmltoimage --quality 75 https://www.nowcoder.com D:/work/data/wk-images/3.png";
        try {
            Runtime.getRuntime().exec(cmd);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
