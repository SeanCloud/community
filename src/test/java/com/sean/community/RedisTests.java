package com.sean.community;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTests {
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Test
    public void testStrings(){
        String redisKey = "test:count";
        redisTemplate.opsForValue().set(redisKey, 1);
        System.out.println(redisTemplate.opsForValue().get(redisKey));
        System.out.println(redisTemplate.opsForValue().increment(redisKey));
        System.out.println(redisTemplate.opsForValue().decrement(redisKey));
    }

    @Test
    public void testHash(){
        String redisKey = "test:user";
        redisTemplate.opsForHash().put(redisKey, "id", 1);
        redisTemplate.opsForHash().put(redisKey, "username", "Sean");

        System.out.println(redisTemplate.opsForHash().get(redisKey, "id"));
        System.out.println(redisTemplate.opsForHash().get(redisKey, "username"));
    }

    @Test
    public void testList(){
        String redisKey = "test:ids";
        redisTemplate.opsForList().leftPush(redisKey, 101);
        redisTemplate.opsForList().leftPush(redisKey, 102);
        redisTemplate.opsForList().leftPush(redisKey, 103);

        System.out.println(redisTemplate.opsForList().size(redisKey));
        System.out.println(redisTemplate.opsForList().index(redisKey, 0));
        System.out.println(redisTemplate.opsForList().range(redisKey, 0, 2));

        System.out.println(redisTemplate.opsForList().leftPop(redisKey));
        System.out.println(redisTemplate.opsForList().leftPop(redisKey));
        System.out.println(redisTemplate.opsForList().leftPop(redisKey));
    }

    @Test
    public void testSet(){
        String redisKey = "test:teachers";
        redisTemplate.opsForSet().add(redisKey, "刘备", "Sean", "kiwi");
        System.out.println(redisTemplate.opsForSet().size(redisKey));
        System.out.println(redisTemplate.opsForSet().pop(redisKey));
        System.out.println(redisTemplate.opsForSet().members(redisKey));
    }

    @Test
    public void testSortedSet(){
        String redisKey = "test:student";
        redisTemplate.opsForZSet().add(redisKey, "Sean", 80);
        redisTemplate.opsForZSet().add(redisKey, "kiwi", 90);
        redisTemplate.opsForZSet().add(redisKey, "jinx", 50);
        redisTemplate.opsForZSet().add(redisKey, "ekko", 70);
        redisTemplate.opsForZSet().add(redisKey, "Darius", 70);
        System.out.println(redisTemplate.opsForZSet().zCard(redisKey));
        System.out.println(redisTemplate.opsForZSet().score(redisKey, "Sean"));
        System.out.println(redisTemplate.opsForZSet().reverseRank(redisKey, "ekko"));   // 返回索引
        System.out.println(redisTemplate.opsForZSet().range(redisKey, 0, 2)); // 从小到大
    }

    @Test
    public void testKeys() throws InterruptedException {
        redisTemplate.delete("test:user");
        System.out.println(redisTemplate.hasKey("test:user"));
        redisTemplate.expire("test:count", 10, TimeUnit.SECONDS);
        System.out.println(redisTemplate.hasKey("test:count"));
        Thread.sleep(10000);
        System.out.println(redisTemplate.hasKey("test:count"));
    }

    // 多次访问同一个 redisKey 使用绑定对象
    @Test
    public void testBoundOperations(){
        String redisKey = "test:count";
        BoundValueOperations<String, Object> operations = redisTemplate.boundValueOps(redisKey);
        operations.increment();
        operations.increment();
        operations.increment();
        operations.increment();
        operations.increment();
        System.out.println(operations.get());
    }

    // 编程式事务
    // Redis 启用事务之后，再去执行命令，不会立刻执行，而是把命令放到队列里存着
    // 提交事务时再把命令发给 redis 服务器一起执行
    // 所以事务中间的查询输出是没有用的
    @Test
    public void testTransactional(){
        Object object = redisTemplate.execute(new SessionCallback<Object>() {
            @Override
            public Object execute(RedisOperations operations) throws DataAccessException {
                String redisKey = "test:tx";
                operations.multi();         // 开启事务
                operations.opsForSet().add(redisKey, "sean");
                operations.opsForSet().add(redisKey, "kiwi");
                operations.opsForSet().add(redisKey, "ekko");
                System.out.println(operations.opsForSet().members(redisKey));   // 事务中间的查询是没用的
                return operations.exec();   // 提交事务
            }
        });
        System.out.println(object);
    }

    // 测试 HyperLogLog
    // 统计20w个重复数据的独立总数
    // 测试结果：99565
    @Test
    public void testHyperLogLog(){
        String redisKey = "test:HyperLogLog:01";
        for(int i = 0; i < 100000; i++){
            redisTemplate.opsForHyperLogLog().add(redisKey, i);
        }
        for(int i = 0; i < 100000; i++){
            int r = (int)(Math.random() * 100000 + 1);  // [1, 1000000)
            redisTemplate.opsForHyperLogLog().add(redisKey, r);
        }
        System.out.println(redisTemplate.opsForHyperLogLog().size(redisKey));
    }

    // 测试 hyperLogLog 合并数据
    // 比如统计一周的 UV，合并七天的 UV 然后统计
    // 测试结果：19891
    @Test
    public void testHyperLogLogUnion(){
        String redisKey = "test:HyperLogLog:02";
        for (int i = 1; i <= 10000; i++){
            redisTemplate.opsForHyperLogLog().add(redisKey, i);
        }
        String redisKey1 = "test:HyperLogLog:03";
        for (int i = 5001; i <= 15000; i++){
            redisTemplate.opsForHyperLogLog().add(redisKey, i);
        }
        String redisKey2 = "test:HyperLogLog:04";
        for (int i = 10001; i <= 20000; i++){
            redisTemplate.opsForHyperLogLog().add(redisKey, i);
        }
        // 一共 3w 数据，不重复的有 2w
        String redisKeyUnion = "test:HyperLogLog:union";
        redisTemplate.opsForHyperLogLog().union(redisKeyUnion, redisKey, redisKey1, redisKey2);
        System.out.println(redisTemplate.opsForHyperLogLog().size(redisKeyUnion));
    }
    // 测试 bitmap
    // 统计一组数据的布尔值
    @Test
    public void testBitmap(){
        String redisKey = "test:bitmap:01";
        // 记录
        redisTemplate.opsForValue().setBit(redisKey, 1, true);  // 第一位存 1
        redisTemplate.opsForValue().setBit(redisKey, 4, true);  // 第 4 位存 1
        redisTemplate.opsForValue().setBit(redisKey, 7, true);  // 第 7 位存 1
        // 查询
        System.out.println(redisTemplate.opsForValue().getBit(redisKey, 0));
        System.out.println(redisTemplate.opsForValue().getBit(redisKey, 1));
        System.out.println(redisTemplate.opsForValue().getBit(redisKey, 2));
        // 统计 true 的数量
        Object object = redisTemplate.execute(new RedisCallback<Object>() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                // JRedis 客户端在向 Redis 发命令时，底层是用的字节流输出
                // 该函数对应的命令是 BitCount sign [start, end]
                return connection.bitCount(redisKey.getBytes());
            }
        });
        System.out.println(object);
    }
    // 统计 3 组数据的布尔值，并对这 3 组数据进行 OR 运算
    // 测试结果： 1110000 | 0011100 | 0000111 = 1111111
    @Test
    public void testBitmapOperation(){
        // 1110000
        String redisKey2 = "test:bitmap:02";
        redisTemplate.opsForValue().setBit(redisKey2, 0, true);
        redisTemplate.opsForValue().setBit(redisKey2, 1, true);
        redisTemplate.opsForValue().setBit(redisKey2, 2, true);
        // 0011100
        String redisKey3 = "test:bitmap:03";
        redisTemplate.opsForValue().setBit(redisKey2, 2, true);
        redisTemplate.opsForValue().setBit(redisKey2, 3, true);
        redisTemplate.opsForValue().setBit(redisKey2, 4, true);
        // 0000111
        String redisKey4 = "test:bitmap:04";
        redisTemplate.opsForValue().setBit(redisKey2, 4, true);
        redisTemplate.opsForValue().setBit(redisKey2, 5, true);
        redisTemplate.opsForValue().setBit(redisKey2, 6, true);


        String redisKey = "test:bitmap:or";
        Object object = redisTemplate.execute(new RedisCallback<Object>() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                connection.bitOp(
                        RedisStringCommands.BitOperation.OR,
                        redisKey.getBytes(),
                        redisKey2.getBytes(),
                        redisKey3.getBytes(),
                        redisKey4.getBytes()
                );
                return connection.bitCount(redisKey.getBytes());
            }
        });

        System.out.println(object);
        System.out.println(redisTemplate.opsForValue().getBit(redisKey, 0));
        System.out.println(redisTemplate.opsForValue().getBit(redisKey, 1));
        System.out.println(redisTemplate.opsForValue().getBit(redisKey, 2));
        System.out.println(redisTemplate.opsForValue().getBit(redisKey, 3));
        System.out.println(redisTemplate.opsForValue().getBit(redisKey, 4));
        System.out.println(redisTemplate.opsForValue().getBit(redisKey, 5));
        System.out.println(redisTemplate.opsForValue().getBit(redisKey, 6));
    }
}
