package com.sean.community.comtroller;

import com.sean.community.service.DataStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

@Controller
public class DataStatisticsController {
    private DataStatisticsService dataStatisticsService;

    @Autowired
    public void setDataStatisticsService(DataStatisticsService dataStatisticsService) {
        this.dataStatisticsService = dataStatisticsService;
    }

    @RequestMapping(path = "/dataStatistics", method = {RequestMethod.GET, RequestMethod.POST})
    public String getDataStatisticsPage(){
        return "/site/admin/data";
    }

    // 统计网站 UV
    @RequestMapping(path = "/dataStatistics/uv", method = RequestMethod.POST)
    public String calculateUV(Model model,
                              @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                              @DateTimeFormat(pattern = "yyyy-MM-dd") Date end){
        long uv = dataStatisticsService.calculateUV(start, end);
        model.addAllAttributes(new ModelMap("uvResult", uv));
        model.addAllAttributes(new ModelMap("uvStartDate", start));
        model.addAllAttributes(new ModelMap("uvEndDate", end));
        return "forward:/dataStatistics";    // 直接写字符串是返回给模板返回给 DispatchServlet, forward 是转发给方法，转发还是 POST
    }

    // 统计网站 UV
    @RequestMapping(path = "/dataStatistics/dau", method = RequestMethod.POST)
    public String calculateDAU(Model model,
                              @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
                              @DateTimeFormat(pattern = "yyyy-MM-dd") Date end){
        long dau = dataStatisticsService.calculateDAU(start, end);
        model.addAllAttributes(new ModelMap("dauResult", dau));
        model.addAllAttributes(new ModelMap("dauStartDate", start));
        model.addAllAttributes(new ModelMap("dauEndDate", end));
        return "forward:/dataStatistics";
    }

}
