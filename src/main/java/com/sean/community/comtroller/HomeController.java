package com.sean.community.comtroller;

import com.sean.community.entity.DiscussPost;
import com.sean.community.entity.Page;
import com.sean.community.service.DiscussPostService;
import com.sean.community.service.LikeService;
import com.sean.community.service.UserService;
import com.sean.community.util.CommunityConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 首页控制器层
 */
@Controller
public class HomeController implements CommunityConstant {
    private DiscussPostService discussPostService;
    private UserService userService;
    private LikeService likeService;

    @Autowired
    public void setDiscussPostService(DiscussPostService discussPostService) {
        this.discussPostService = discussPostService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setLikeService(LikeService likeService) {
        this.likeService = likeService;
    }

    /**
     * 首页访问映射
     * @param model: 数据模型
     * @param page： 分页组件
     * @return: 返回视图到 index.html
     */
    @RequestMapping({"/index", "/", "index.html"})
    public String getIndexPage(Model model, Page page, @RequestParam(name = "orderMode", defaultValue = "0") int orderMode){
        // 为什么 page 无需通过 model 返回给页面？？
        // 方法调用之前，springMVC 的 dispatchServlet 会自动初始化 Model 和 page
        // 并且把 page 装到 model 里面
        // 分页
        page.setRows(discussPostService.findDiscussPostRows(0));    // 0 查询所有行
        page.setPath("/index?orderMode=" + orderMode);

        List<DiscussPost> list = discussPostService
                .findDiscussPosts(0, page.getOffset(), page.getLimit(), orderMode);
        List<Map<String, Object>> discussPosts = new ArrayList<>();
        if(list != null){
            for(DiscussPost post : list){
                Map<String, Object> map = new HashMap<>();
                map.put("post", post);
                map.put("user", userService.findUserById(post.getUserId()));
                map.put("likeCount", likeService.findEntityLikeCount(ENTITY_TYPE_DISCUSS_POST, post.getId()));
                discussPosts.add(map);
            }
        }
        model.addAllAttributes(new ModelMap("discussPosts", discussPosts));
        model.addAllAttributes(new ModelMap("orderMode", orderMode));
        return "index";
    }

    @RequestMapping(path = "/error500", method = RequestMethod.GET)
    public String getError500(){
        return "/error/500";
    }

    @RequestMapping(path = "/denied", method = RequestMethod.GET)
    public String getDeniedPage(){
        return "/error/404";
    }
}
