package com.sean.community.comtroller;

import com.sean.community.entity.*;
import com.sean.community.event.EventProducer;
import com.sean.community.service.CommentService;
import com.sean.community.service.DiscussPostService;
import com.sean.community.service.LikeService;
import com.sean.community.service.UserService;
import com.sean.community.util.CommunityConstant;
import com.sean.community.util.CommunityUtil;
import com.sean.community.util.HostHolder;
import com.sean.community.util.RedisKeyUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("/discussPost")
public class DiscussPostController implements CommunityConstant {
    private DiscussPostService discussPostService;
    private CommentService commentService;
    private UserService userService;
    private HostHolder hostHolder;
    private LikeService likeService;
    private EventProducer eventProducer;
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public void setDiscussPostService(DiscussPostService discussPostService) {
        this.discussPostService = discussPostService;
    }

    @Autowired
    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setHostHolder(HostHolder hostHolder) {
        this.hostHolder = hostHolder;
    }

    @Autowired
    public void setLikeService(LikeService likeService) {
        this.likeService = likeService;
    }

    @Autowired
    public void setEventProducer(EventProducer eventProducer) {
        this.eventProducer = eventProducer;
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    @ResponseBody
    public String addDiscussPost(String title, String content){
        User user = hostHolder.getUser();
        if(user == null){
            return CommunityUtil.getJSONString(403, "用户还未登录!");
        }
        DiscussPost discussPost = new DiscussPost();
        discussPost.setUserId(user.getId());
        discussPost.setTitle(title);
        discussPost.setContent(content);
        discussPost.setCreateTime(new Date());
        discussPostService.addDiscussPost(discussPost);
        // 触发发帖事件：将帖子同步到 es 服务器中
        Event event = new Event()
                .setTopic(TOPIC_PUBLISH)
                .setUserId(user.getId())
                .setEntityType(ENTITY_TYPE_DISCUSS_POST)
                .setEntityId(discussPost.getId());
        eventProducer.fireEvent(event);

        // 将分数有变化的帖子缓存到 Redis 中，用于计算帖子分数
        String postScoreKey = RedisKeyUtil.getPostScoreKey();
        redisTemplate.opsForSet().add(postScoreKey, discussPost.getId());       // 将帖子 id 缓存到 set 中

        // 报错以后统一处理
        return CommunityUtil.getJSONString(200, "发布成功！");
    }

    @RequestMapping(path = "/detail/{discussPostId}", method = RequestMethod.GET)
    public String getDiscussPost(@PathVariable("discussPostId") int discussPostId, Model model, Page page){
        // 帖子
        DiscussPost discussPost = discussPostService.findDiscussPostById(discussPostId);
        // 如果改帖子已经被删除了，则不应该被显示
        if(discussPost == null || discussPost.getStatus() == DISCUSSPOST_STATUS_BLACKLIST){
            throw new RuntimeException("帖子不存在!");
        }
        model.addAllAttributes(new ModelMap("discussPost", discussPost));
        // 作者
        User discussAuthor = userService.findUserById(discussPost.getUserId());
        model.addAllAttributes(new ModelMap("discussAuthor", discussAuthor));
        // 帖子的点赞数量
        Long discussLikeCount = likeService.findEntityLikeCount(ENTITY_TYPE_DISCUSS_POST, discussPostId);
        model.addAllAttributes(new ModelMap("discussLikeCount", discussLikeCount));
        // 当前用户对帖子的点赞状态
        int discussLikeStatus = hostHolder.getUser() == null ? 0 :
                likeService.findEntityLikeStatus(hostHolder.getUser().getId(), ENTITY_TYPE_DISCUSS_POST, discussPostId);
        model.addAllAttributes(new ModelMap("discussLikeStatus", discussLikeStatus));
        // 帖子评论
        // 分页查询
        page.setLimit(5);
        page.setPath("/discussPost/detail/" + discussPostId);
        page.setRows(discussPost.getCommentCount());    // 评论总数，从帖子表中的冗余字段获得，避免了统计评论表
        // 命名定义
        //  - 评论：给帖子的评论
        //  - 回复：给评论的评论
        // 评论列表
        List<Comment> commentList = commentService.findCommentByEntity(
                ENTITY_TYPE_DISCUSS_POST, discussPost.getId(), page.getOffset(), page.getLimit());
        // 评论记录里有 userId 需要外键关联为 User
        // 评论的 ViewObject 列表
        List<Map<String, Object>>commentVoList = new ArrayList<>();
        if(commentList != null){
            for(Comment comment : commentList){
                // 评论 ViewObject
                Map<String, Object> commentVo = new HashMap<>();
                // 评论
                commentVo.put("comment", comment);
                // 评论的作者
                commentVo.put("commentUser", userService.findUserById(comment.getUserId()));
                // 评论的点赞数量
                commentVo.put("commentLikeCount", likeService.findEntityLikeCount(ENTITY_TYPE_COMMENT, comment.getId()));
                // 当前登录用户对评论的点赞状态
                int commentLikeStatus = hostHolder.getUser() == null ? 0 :
                        likeService.findEntityLikeStatus(hostHolder.getUser().getId(), ENTITY_TYPE_COMMENT, comment.getId());
                commentVo.put("commentLikeStatus", commentLikeStatus);
                // 回复列表
                List<Comment> replyList = commentService.findCommentByEntity(ENTITY_TYPE_COMMENT, comment.getId(), 0, Integer.MAX_VALUE);
                // 回复的 ViewObject 列表
                List<Map<String, Object>>replyListVoList = new ArrayList<>();
                if(replyList != null){
                    for(Comment reply : replyList){
                        // 回复 ViewObject
                        Map<String, Object> replyVo = new HashMap<>();
                        // 回复
                        replyVo.put("reply", reply);
                        // 回复的作者
                        replyVo.put("replyUser", userService.findUserById(reply.getUserId()));
                        // 回复的点赞数量
                        replyVo.put("replyLikeCount", likeService.findEntityLikeCount(ENTITY_TYPE_COMMENT, reply.getId()));
                        // 当前登录用户对回复的点赞状态
                        int replyLikeStatus = hostHolder.getUser() == null ? 0 :
                                likeService.findEntityLikeStatus(hostHolder.getUser().getId(), ENTITY_TYPE_COMMENT, reply.getId());
                        replyVo.put("replyLikeStatus", replyLikeStatus);
                        // 回复的目标 targetId
                        User targetUser = reply.getTargetId() == 0 ? null : userService.findUserById(reply.getTargetId());
                        replyVo.put("targetUser", targetUser);
                        // 添加回复 VO 到回复 VO 列表
                        replyListVoList.add(replyVo);
                    }
                }
                // 将回复列表评论 VO 里面
                commentVo.put("replyVoList", replyListVoList);
                // 回复的数量
                commentVo.put("replyNum", replyListVoList.size());
                // 添加评论 VO 到评论列表
                commentVoList.add(commentVo);
            }
            model.addAllAttributes(new ModelMap("commentVoList", commentVoList));
        }
        return "site/discuss-detail";
    }

    // 置顶帖子：异步
    @RequestMapping(path = "/top", method = RequestMethod.POST)
    @ResponseBody
    public String setDiscussPostTop(int discussPostId){
        discussPostService.updateTypeById(discussPostId, DISCUSSPOST_TYPE_TOPPING);
        // 同步到 es
        // 触发发帖事件：将帖子同步到 es 服务器中
        Event event = new Event()
                .setTopic(TOPIC_PUBLISH)
                .setUserId(hostHolder.getUser().getId())
                .setEntityType(ENTITY_TYPE_DISCUSS_POST)
                .setEntityId(discussPostId);
        eventProducer.fireEvent(event);

        return CommunityUtil.getJSONString(200);
    }

    // 加精帖子：异步
    @RequestMapping(path = "/refine", method = RequestMethod.POST)
    @ResponseBody
    public String setDiscussPostRefine(int discussPostId){
        discussPostService.updateStatusById(discussPostId, DISCUSSPOST_STATUS_REFINE);
        // 同步到 es
        // 触发发帖事件：将帖子同步到 es 服务器中
        Event event = new Event()
                .setTopic(TOPIC_PUBLISH)
                .setUserId(hostHolder.getUser().getId())
                .setEntityType(ENTITY_TYPE_DISCUSS_POST)
                .setEntityId(discussPostId);
        eventProducer.fireEvent(event);


        // 将分数有变化的帖子缓存到 Redis 中，用于计算帖子分数
        String postScoreKey = RedisKeyUtil.getPostScoreKey();
        redisTemplate.opsForSet().add(postScoreKey, discussPostId);       // 将帖子 id 缓存到 set 中

        return CommunityUtil.getJSONString(200);
    }

    // 删除帖子：异步
    @RequestMapping(path = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public String setDiscussPostBlacklist(int discussPostId){
        discussPostService.updateStatusById(discussPostId, DISCUSSPOST_STATUS_BLACKLIST);
        // 同步到 es
        // 触发删帖事件：将帖子从 es 服务器中删除
        Event event = new Event()
                .setTopic(TOPIC_DELETE)
                .setUserId(hostHolder.getUser().getId())
                .setEntityType(ENTITY_TYPE_DISCUSS_POST)
                .setEntityId(discussPostId);
        eventProducer.fireEvent(event);

        return CommunityUtil.getJSONString(200);
    }
}
