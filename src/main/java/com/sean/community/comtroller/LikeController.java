package com.sean.community.comtroller;

import com.sean.community.annotation.LoginRequired;
import com.sean.community.entity.Event;
import com.sean.community.entity.User;
import com.sean.community.event.EventProducer;
import com.sean.community.service.LikeService;
import com.sean.community.util.CommunityConstant;
import com.sean.community.util.CommunityUtil;
import com.sean.community.util.HostHolder;
import com.sean.community.util.RedisKeyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class LikeController implements CommunityConstant {
    private LikeService likeService;
    private HostHolder hostHolder;
    private EventProducer producer;
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public void setLikeService(LikeService likeService) {
        this.likeService = likeService;
    }

    @Autowired
    public void setHostHolder(HostHolder hostHolder) {
        this.hostHolder = hostHolder;
    }

    @Autowired
    public void setProducer(EventProducer producer) {
        this.producer = producer;
    }

    @Autowired
    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @RequestMapping(path = "/like", method = RequestMethod.POST)
    @ResponseBody
    // 异步请求不能使用 @LoginRequested 注解
    public String like(int entityType, int entityId, int entityAuthorId, int discussPostId){
        // 获取当前用户
        User user = hostHolder.getUser();
        if(user == null){
            return CommunityUtil.getJSONString(403, "用户还未登录!");
        }
        // 点赞
        likeService.like(user.getId(), entityType, entityId, entityAuthorId);
        // 点赞数量
        long likeCount = likeService.findEntityLikeCount(entityType, entityId);
        // 点赞状态
        int likeStatus = likeService.findEntityLikeStatus(user.getId(), entityType, entityId);
        // 返回结果
        Map<String, Object> map = new HashMap<>();
        map.put("likeCount", likeCount);
        map.put("likeStatus", likeStatus);
        String jsonString = CommunityUtil.getJSONString(200, null, map);
        // 触发点赞事件（取消赞不能通知）
        if (entityAuthorId != user.getId()) { // 对自己点赞不通知
            if(likeStatus == LIKE_STATUS_LIKED){    // 点赞了
                Event event = new Event()
                        .setTopic(TOPIC_LIKE)
                        .setUserId(hostHolder.getUser().getId())
                        .setEntityType(entityType)
                        .setEntityId(entityId)
                        .setEntityUserId(entityAuthorId)
                        .setData("discussPostId", discussPostId);   // 帖子 id： 用于被通知用户可以定位到帖子
                producer.fireEvent(event);
            }
        }
        if (entityType == ENTITY_TYPE_DISCUSS_POST) {
            // 将分数有变化的帖子缓存到 Redis 中，用于计算帖子分数
            String postScoreKey = RedisKeyUtil.getPostScoreKey();
            redisTemplate.opsForSet().add(postScoreKey, discussPostId); // 将帖子 id 缓存到 set 中
        }
        return jsonString;
    }
}
