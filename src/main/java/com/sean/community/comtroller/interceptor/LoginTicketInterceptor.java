package com.sean.community.comtroller.interceptor;

import com.sean.community.entity.LoginTicket;
import com.sean.community.entity.User;
import com.sean.community.service.UserService;
import com.sean.community.util.CookieUtil;
import com.sean.community.util.HostHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 登录拦截器，拦截所有请求，检查是否登录
 */
@Component
public class LoginTicketInterceptor implements HandlerInterceptor {
    UserService userService;
    HostHolder hostHolder;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setHostHolder(HostHolder hostHolder) {
        this.hostHolder = hostHolder;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 获取 cookie 中的 ticket
        String ticket = CookieUtil.getValue(request, "ticket");
        if(ticket != null){
            // 查询凭证
            LoginTicket loginTicket = userService.findLoginTicket(ticket);      // 查询数据库效率低 改为 用 Redis 存储
            // 检查凭证是否有效
            if(loginTicket != null && loginTicket.getStatus() == 0 &&
                loginTicket.getExpired().after(new Date())){
                // 根据凭证中的用户 id 查询用户
                User user = userService.findUserById(loginTicket.getUserId());  // 每次都查询数据库，效率低，改为 redis 缓存
                // 在本次请求中持有用户
                // 服务器与浏览器之间的通信是一对多，是多线程的，所以不能用变量存
                // 需要线程隔离
                hostHolder.setUser(user);
                // 构建用户认证结果，并且存入 SecurityContext，以便于 Security 进行授权
                Authentication authentication =
                        new UsernamePasswordAuthenticationToken(
                                user,
                                user.getPassword(),
                                userService.getAuthorities(user.getId())
                        );
                // 通过 SecurityContextHolder 将凭证存入 SecurityContext
                SecurityContextHolder.setContext(new SecurityContextImpl(authentication));
            }
        }
        return true;
    }

    // 在模板之前调用
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        User user = hostHolder.getUser();
        if(user != null && modelAndView != null){
            modelAndView.addObject("loginUser", user);
        }
    }

    // 在模板之后
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        hostHolder.clear();
        // 由于拦截器工作在 Field 之后，所以凭证不能删除
        // 不然下一次请求将会被 Security 拦截
        // SecurityContextHolder.clearContext();
        // 疑问：
        // 1. 如果不清除，一个请求就会set一次会怎么样？
        // 2. 在登录时就把凭证存入 SecurityContext 会不会更好？
        // 3. 把拦截器（this）的代码挪到 filter 里，然后这个 filter 在那个权限检查的 filter 之前，但是我不知道权限检查的 filter 是哪个？
    }
}
