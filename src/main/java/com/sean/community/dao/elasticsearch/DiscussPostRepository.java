package com.sean.community.dao.elasticsearch;

import com.sean.community.entity.DiscussPost;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
// ElasticsearchRepository 泛型 <处理实体类，主键类型>
public interface DiscussPostRepository extends ElasticsearchRepository<DiscussPost, Integer> {

}
