package com.sean.community.dao;

import com.sean.community.entity.Message;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MessageMapper {

    // 会话列表
    /**
     * 查询当前用户的会话列表，针对每个会话，只返回一条最新消息
     * @param userId 当前用户 id
     * @param offset 分页偏移
     * @param limit 分页条数
     * @return 会话列表
     */
    List<Message> selectConversations(int userId, int offset, int limit);

    /**
     * 返回总的会话列表数量
     * @param userId 当前用户 id
     * @return 总的会话列表数
     */
    int selectConversationCount(int userId);

    // 详情列表
    /**
     * 查询某个会话所包含的私信列表
     * @param conversationId 会话 id
     * @param offset 分页偏移
     * @param limit 分页条数
     * @return 私信列表
     */
    List<Message> selectLetters(String conversationId, int offset, int limit);

    /**
     * 查询某个会话所包含的私信总数
     * @param conversationId 会话id
     * @return 总的私信数
     */
    int selectLetterCount(String conversationId);

    /**
     * 查询未读消息数量
     * @param userId 用户 id
     * @param conversationId 会话 id， 如果是空值则查询所有会话未读消息数量
     * @return 未读消息数量
     */
    int selectLetterUnreadCount(int userId, String conversationId);

    /**
     * 新增一条消息
     * @param message 消息
     * @return 影响行数
     */
    int insertMessage(Message message);

    /**
     * 修改信息的状态
     * @param idList 消息 id
     * @param status 状态
     * @return 影响行数
     */
    int updateStatus(List<Integer> idList, int status);

    /**
     * 查询某个主题下最新的通知
     * @param userId 用户 id
     * @param topic 主题
     * @return 最新通知消息
     */
    Message selectLatestNotice(int userId, String topic);

    /**
     * 查询某个主题下的通知数量
     * @param userId 用户 id
     * @param topic 主题
     * @return 通知数量
     */
    int selectNoticeCount(int userId, String topic);

    /**
     * 查询未读的通知数量
     * @param userId 用户 id
     * @param topic 主题
     * @return 未读通知数量
     */
    int selectNoticeUnreadCount(int userId, String topic);

    /**
     * 查询某个主题包含的通知详情列表
     * @param userId 用户 id
     * @param topic 主题
     * @param offset 分页偏移
     * @param limit 分页限制
     * @return 通知列表
     */
    List<Message> selectNotices(int userId, String topic, int offset, int limit);
}
