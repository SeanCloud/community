package com.sean.community.dao;

import com.sean.community.entity.Comment;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 评论表，数据访问层接口
 */
@Mapper
@Repository
public interface CommentMapper {
    /**
     * 根据实体查询评论，有分页功能
     * @param entityType 实体类型 1 帖子，2 评论
     * @param entityId 实体 id
     * @param offset 偏移
     * @param limit 查询列数
     * @return 返回评论集合
     */
    List<Comment> selectCommentsByEntity(int entityType, int entityId, int offset, int limit);

    /**
     * 根据实体查训总评论数
     * @param entityType 实体类型
     * @param entityId 实体 id
     * @return 总评论数
     */
    int selectCountByEntity(int entityType, int entityId);

    /**
     * 插入评论
     * @param comment 评论实体类
     * @return 影响行数
     */
    int insertComment(Comment comment);

    /**
     * 根据 id 查评论
     * @param id 评论 id
     * @return 评论
     */
    Comment selectCommentById(int id);

    /**
     * 根据用户 id 查询帖子的评论
     * @param userId 用户 id
     * @param offset 编译
     * @param limit 查询列数
     * @return 返回评论集合
     */
    List<Comment> selectDiscussPostCommentByUser(int userId, int offset, int limit);

    /**
     * 根据用户 id 查询帖子的评论的总数
     * @param userId 用户 id
     * @return 评论总数
     */
    int selectDiscussPostCommentCountByUser(int userId);
}
