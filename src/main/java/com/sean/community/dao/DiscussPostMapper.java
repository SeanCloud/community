package com.sean.community.dao;

import com.sean.community.entity.DiscussPost;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 帖子表，数据访问接口
 */
@Mapper
@Repository
public interface DiscussPostMapper {
    /**
     * 查询帖子
     * @param userId: 查询所有帖子(userId = 0), 查询某个用户的帖子
     * @param offset: 分页, 起始行号
     * @param limit: 分页, 每页显示数量
     * @param orderMode: 排序模式，0 默认排序（时间倒序），1 帖子热度分数排序
     * @return: 帖子列表
     */
    List<DiscussPost> selectDiscussPosts(int userId, int offset, int limit, int orderMode);

    /**
     * 查询帖子数量, 用于分页计算一共有多少页
     * @param userId: 查询所有帖子(userId = 0), 查询某个用户的帖子
     * @return: 帖子列表
     */
    int selectDiscussPostRows(@Param("userId") int userId);

    /**
     * 插入帖子
     * @param discussPost 帖子实体
     * @return 影响行数
     */
    int insertDiscussPost(DiscussPost discussPost);

    /**
     * 查询帖子详情
     * @param id 帖子 id
     * @return 帖子实体类
     */
    DiscussPost selectDiscussPostById(int id);

    /**
     * 根据帖子 id 更新帖子的评论数量
     * @param commentCount 评论数量
     * @return 影响行数
     */
    int updateCommentCount(int id, int commentCount);

    int updateTypeById(int id, int type);

    int updateStatusById(int id, int status);

    int updateScoreById(int id, double score);
}
