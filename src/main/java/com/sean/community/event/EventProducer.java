package com.sean.community.event;

import com.alibaba.fastjson.JSONObject;
import com.sean.community.entity.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * 生产者
 */
@Component
@SuppressWarnings("all")
public class EventProducer {
    private KafkaTemplate kafkaTemplate;

    @Autowired
    public void setKafkaTemplate(KafkaTemplate kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    /**
     * 处理事件
     * @param event 事件实体
     */
    public void fireEvent(Event event){
        // 将事件发布到指定主题 (消息是 json 字符串，方便消费者还原为 event 对象)
        kafkaTemplate.send(event.getTopic(), JSONObject.toJSONString(event));
    }
}
