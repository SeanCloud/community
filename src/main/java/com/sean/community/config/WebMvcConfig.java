package com.sean.community.config;

import com.sean.community.comtroller.interceptor.DataStatisticsInterceptor;
import com.sean.community.comtroller.interceptor.LoginRequiredInterceptor;
import com.sean.community.comtroller.interceptor.LoginTicketInterceptor;
import com.sean.community.comtroller.interceptor.MessageInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    private LoginTicketInterceptor loginTicketInterceptor;
//    登录拦截器，2022.05.09 废弃，使用 Spring Security 代替
//    private LoginRequiredInterceptor loginRequiredInterceptor;
    private MessageInterceptor messageInterceptor;
    private DataStatisticsInterceptor dataStatisticsInterceptor;

    @Autowired
    public void setLoginTicketInterceptor(LoginTicketInterceptor loginTicketInterceptor) {
        this.loginTicketInterceptor = loginTicketInterceptor;
    }

//    登录拦截器，2022.05.09 废弃，使用 Spring Security 代替
//    @Autowired
//    public void setLoginRequiredInterceptor(LoginRequiredInterceptor loginRequiredInterceptor) {
//        this.loginRequiredInterceptor = loginRequiredInterceptor;
//    }

    @Autowired
    public void setMessageInterceptor(MessageInterceptor messageInterceptor) {
        this.messageInterceptor = messageInterceptor;
    }

    @Autowired
    public void setDataStatisticsInterceptor(DataStatisticsInterceptor dataStatisticsInterceptor) {
        this.dataStatisticsInterceptor = dataStatisticsInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginTicketInterceptor)
                .excludePathPatterns("/**/*.css", "/**/*.js",
                        "/**/*.png", "/**/*.jpg", "/**/*.jpeg");
//        登录拦截器，2022.05.09 废弃，使用 Spring Security 代替
//        registry.addInterceptor(loginRequiredInterceptor)
//                .excludePathPatterns("/**/*.css", "/**/*.js",
//                        "/**/*.png", "/**/*.jpg", "/**/*.jpeg");
        registry.addInterceptor(messageInterceptor)
                .excludePathPatterns("/**/*.css", "/**/*.js",
                        "/**/*.png", "/**/*.jpg", "/**/*.jpeg");
        registry.addInterceptor(dataStatisticsInterceptor)
                .excludePathPatterns("/**/*.css", "/**/*.js",
                        "/**/*.png", "/**/*.jpg", "/**/*.jpeg");
    }
}
