package com.sean.community.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.File;

@Configuration // Spring 容器在启动时，就会加载配置类
public class WkConfig {
    private static final Logger logger = LoggerFactory.getLogger(WkConfig.class);

    @Value("${wk.image.command}")
    private String wkCommand;

    @Value("${wk.image.storage}")
    private String wkImageStorage;

    @SuppressWarnings("all")
    @PostConstruct  // 加载完毕会调用 postConstruct
    public void init(){
        // 创建图片存储目录
        File file = new File(wkImageStorage);
        if(!file.exists()){
            file.mkdirs();
            logger.info("创建 wk 图片存储文件夹成功！" + wkImageStorage);
        }
    }
}
