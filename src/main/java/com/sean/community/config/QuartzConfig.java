package com.sean.community.config;

// import com.sean.community.quartz.TestJob;
import com.sean.community.quartz.PostScoreRefreshJob;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

/**
 * 配置 quartz 的任务，
 * 只在第一次时生效，
 * 之后 quartz 通过数据库执行任务
 */
@Configuration
public class QuartzConfig {
    // FactoryBean: 简化 Bean 的实例话过程
    // 1. 通过 FactoryBean 封装了 Bean 的实例化过程
    // 2. 可以将 FactoryBean 装配到容器中
    // 3. 将 FactoryBean 注入给其他的 Bean
    // 4. 被注入的 Bean 得到的是 FactoryBean 生产的实例对象

    // 测试quartz：配置 JobDetail
    // @Bean
    /*
    public JobDetailFactoryBean testJobDetail(){
        JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
        jobDetailFactoryBean.setJobClass(TestJob.class);
        jobDetailFactoryBean.setName("testJob");
        jobDetailFactoryBean.setGroup("testJobGroup");
        jobDetailFactoryBean.setDurability(true);   // 长久保存任务
        jobDetailFactoryBean.setRequestsRecovery(true); // 任务是否可恢复
        return jobDetailFactoryBean;
    }
    */

    // 测试quartz：配置 Trigger
    // @Bean
    /*
    public SimpleTriggerFactoryBean testTrigger(JobDetail testJabDetail){
        SimpleTriggerFactoryBean simpleTriggerFactoryBean = new SimpleTriggerFactoryBean();
        simpleTriggerFactoryBean.setJobDetail(testJabDetail);
        simpleTriggerFactoryBean.setName("testTrigger");
        simpleTriggerFactoryBean.setGroup("testTriggerGroup");
        simpleTriggerFactoryBean.setRepeatInterval(3000);   // 执行频率 3000 ms
        simpleTriggerFactoryBean.setJobDataMap(new JobDataMap());   // 存 Job 的底层状态
        return simpleTriggerFactoryBean;
    }
    */

    // 刷新帖子分数任务
    @Bean
    public JobDetailFactoryBean postScoreRefreshJobDetail(){
        JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
        jobDetailFactoryBean.setJobClass(PostScoreRefreshJob.class);
        jobDetailFactoryBean.setName("postScoreRefreshJob");
        jobDetailFactoryBean.setGroup("communityJobGroup");
        jobDetailFactoryBean.setDurability(true);   // 长久保存任务
        jobDetailFactoryBean.setRequestsRecovery(true); // 任务是否可恢复
        return jobDetailFactoryBean;
    }
    @Bean
    public SimpleTriggerFactoryBean postScoreRefreshTrigger(JobDetail postScoreRefreshJobDetail){
        SimpleTriggerFactoryBean simpleTriggerFactoryBean = new SimpleTriggerFactoryBean();
        simpleTriggerFactoryBean.setJobDetail(postScoreRefreshJobDetail);
        simpleTriggerFactoryBean.setName("postScoreRefreshTrigger");
        simpleTriggerFactoryBean.setGroup("communityTriggerGroup");
        simpleTriggerFactoryBean.setRepeatInterval(1000 * 60 * 5);   // 执行频率 5min
        simpleTriggerFactoryBean.setJobDataMap(new JobDataMap());   // 存 Job 的底层状态
        return simpleTriggerFactoryBean;
    }
}
