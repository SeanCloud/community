package com.sean.community.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;

@Configuration
public class ElasticsearchConfig {
    @Value("${spring.elasticsearch.uris}")
    private String esUrl;
    @Bean
    RestHighLevelClient elasticsearchClient(){  // 注意 Bean 的名字冲突
        ClientConfiguration configuration = ClientConfiguration.builder()
                .connectedTo(esUrl).build();
        return RestClients.create(configuration).rest();
    }
}
