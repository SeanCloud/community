package com.sean.community.service;

import com.sean.community.util.RedisKeyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 网站数据统计服务
 */
@Service
public class DataStatisticsService {
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    // 记录
    // 将指定 ip 记录到 UV (HyperLogLog)
    public void recodeUV(String ip){
        String uvKey = RedisKeyUtil.getUVKey(dateFormat.format(new Date()));
        redisTemplate.opsForHyperLogLog().add(uvKey, ip);

    }
    // 将用户id 记录到 DAU (BitMap)
    public void recodeDAU(int userId){
        String dauKey = RedisKeyUtil.getDAUKey(dateFormat.format(new Date()));
        redisTemplate.opsForValue().setBit(dauKey, userId, true);
    }


    // 查看
    // 统计指定日期范围内的 UV
    public long calculateUV(Date start, Date end){
        if(start == null || end == null){
            throw new RuntimeException("参数为空！");
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(start);
        if(calendar.getTime().after(end)){           // 开始日期在结束日期之后
            throw new RuntimeException("参数错误！");
        }
        // Redis Key
        String listKey = RedisKeyUtil.getUVKey(dateFormat.format(start), dateFormat.format(end));   // 合并后 UV 的 key
        List<String> dailyUVKey = new ArrayList<>();    // 每一天 UV 的 key
        while(!calendar.getTime().after(end)){
            dailyUVKey.add(RedisKeyUtil.getUVKey(dateFormat.format(calendar.getTime())));  // 将当前这一天的 key 保存起来
            calendar.add(Calendar.DATE, 1);     // 日期增加一天
        }
        // 合并 hyperLogLog
        redisTemplate.opsForHyperLogLog().union(listKey, dailyUVKey.toArray(new String[0]));
        // 返回统计结果
        return redisTemplate.opsForHyperLogLog().size(listKey);
    }

    // 统计指定日期范围内的 UV
    @SuppressWarnings("all")
    public long calculateDAU(Date start, Date end){
        if(start == null || end == null){
            throw new RuntimeException("参数为空！");
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(start);
        if(calendar.getTime().after(end)){           // 开始日期在结束日期之后
            throw new RuntimeException("参数错误！");
        }
        // Redis Key
        String listKey = RedisKeyUtil.getDAUKey(dateFormat.format(start), dateFormat.format(end));   // 合并后 DAU 的 key
        List<byte[]> dailyDAUKey = new ArrayList<>();    // 每一天 DAU 的 key
        while(!calendar.getTime().after(end)){
            dailyDAUKey.add(RedisKeyUtil.getDAUKey(dateFormat.format(calendar.getTime())).getBytes());  // 将当前这一天的 key 保存起来
            calendar.add(Calendar.DATE, 1);     // 日期增加一天
        }
        // 进行 OR 运算
        return redisTemplate.execute(new RedisCallback<Long>() {
            @Override
            public Long doInRedis(RedisConnection connection) throws DataAccessException {
                connection.bitOp(
                        RedisStringCommands.BitOperation.OR,
                        listKey.getBytes(),
                        dailyDAUKey.toArray(new byte[0][0])
                );
                return connection.bitCount(listKey.getBytes());
            }
        });
    }
}
