package com.sean.community.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.DigestUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CommunityUtil {
    // 生成随机字符串：可用于激活码，上传文件命名等
    public static String generateUUID(){
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    // MD5 加密密码
    // 只能加密，解密只能撞库
    // 都要加盐
    public static String md5(String key){
        if(StringUtils.isBlank(key)){   // null, "", " "
            return null;
        }
        return DigestUtils.md5DigestAsHex(key.getBytes());
    }

    /**
     * 服务器向浏览器返回的 JSON 通常包含，编号，提示信息， 业务数据
     * @param code 编码
     * @param msg 提示信息
     * @param map 业务数据
     * @return JSON 字符串
     */
    public static String getJSONString(int code, String msg, Map<String, Object> map){
        JSONObject json = new JSONObject();
        json.put("code", code);
        json.put("msg", msg);
        if(map != null){
            for(String key : map.keySet()){
                json.put(key, map.get(key));
            }
        }
        return json.toJSONString();
    }
    // 重载
    public static String getJSONString(int code, String msg){
        return getJSONString(code, msg, null);
    }
    // 重载
    public static String getJSONString(int code){
        return getJSONString(code, null, null);
    }

    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "Sean");
        map.put("age", "21");
        System.out.println(getJSONString(0, "ok", map));
    }
}
