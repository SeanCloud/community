package com.sean.community.util;

/**
 * 常量接口
 */
public interface CommunityConstant {
    /**
     * 成功激活
     */
    int ACTIVATION_SUCCESS = 0;

    /**
     * 重复激活
     */
    int ACTIVATION_REPEAT = 1;

    /**
     * 激活失败
     */
    int ACTIVATION_FAILURE = 2;

    /**
     * 默认状态的登录凭证的超时时间
     */
    int DEFAULT_EXPIRED_SECONDS = 3600 * 12;

    /**
     * 勾选记住我的登录凭证的超时时间
     */
    int REMEMBER_EXPIRED_SECONDS = 3600 * 24 * 30;

    /**
     * 实体类型 1： 帖子
     */
    int ENTITY_TYPE_DISCUSS_POST = 1;

    /**
     * 实体类型 2： 评论
     */
    int ENTITY_TYPE_COMMENT = 2;

    /**
     * 实体类型 3： 用户
     */
    int ENTITY_TYPE_USER = 3;

    /**
     * 消息未读
     */
    int MESSAGE_STATUS_UNREAD = 0;

    /**
     * 消息已读
     */
    int MESSAGE_STATUS_READ = 1;

    /**
     * 主题：评论
     */
    String TOPIC_COMMENT = "comment";

    /**
     * 主题：点赞
     */
    String TOPIC_LIKE = "like";

    /**
     * 主题：关注
     */
    String TOPIC_FOLLOW = "follow";

    /**
     * 主题：发帖
     */
    String TOPIC_PUBLISH = "publish";

    /**
     * 主题：删帖
     */
    String TOPIC_DELETE = "delete";

    /**
     * 主题：分享
     */
    String TOPIC_SHARE = "share";

    /**
     * 系统用户 id
     */
    int SYSTEM_USER_ID = 1;

    /**
     * 已赞
     */
    int LIKE_STATUS_LIKED = 1;

    /**
     * 权限：普通用户
     */
    String AUTHORITY_USER = "user";


    /**
     * 权限：管理员
     */
    String AUTHORITY_ADMIN = "admin";


    /**
     * 权限：版主
     */
    String AUTHORITY_MODERATOR = "moderator";

    /**
     * 帖子类型： 普通帖子
     */
    int DISCUSSPOST_TYPE_COMMON = 0;

    /**
     * 帖子类型： 置顶帖子
     */
    int DISCUSSPOST_TYPE_TOPPING = 1;

    /**
     * 帖子状态： 正常
     */
    int DISCUSSPOST_STATUS_NORMAL = 0;

    /**
     * 帖子状态： 精华
     */
    int DISCUSSPOST_STATUS_REFINE = 1;

    /**
     * 帖子状态： 拉黑
     */
    int DISCUSSPOST_STATUS_BLACKLIST = 2;

}
