package com.sean.community.util;

import com.sean.community.entity.User;
import org.springframework.stereotype.Component;

/**
 * 容器作用，用来存放登录的用户，用于代替 session 对象
 * 虽然 session 也是线程隔离的
 */
@Component
public class HostHolder {
    private ThreadLocal<User> userThreadLocal = new ThreadLocal<>();

    public void setUser(User user){
        userThreadLocal.set(user);
    }

    public User  getUser(){
        return userThreadLocal.get();
    }

    public void  clear(){
        userThreadLocal.remove();
    }
}
