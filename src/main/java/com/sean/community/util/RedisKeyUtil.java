package com.sean.community.util;

/**
 * 获取 Redis key 的工具类
 */
public class RedisKeyUtil {
    private static final String SPLIT = ":";
    private static final String PREFIX_ENTITY_LIKE = "like:entity";
    private static final String PREFIX_USER_LIKE = "like:user";
    // A 关注了 B
    private static final String PREFIX_FOLLOWEE = "followee";   // B 是 A 的 followee
    private static final String PREFIX_FOLLOWER = "follower";   // A 是 B 的 follower
    // 使用 Redis 存储验证码
    private static final String PREFIX_KAPTCHA = "kaptcha";
    // 登录凭证
    private static final String PREFIX_TICKET = "ticket";
    // 缓存用户
    private static final String PREFIX_USER = "user";
    // 统计独立访客 Unique Visitors
    private static final String PREFIX_UV = "uv";
    // 统计日活跃用户 Daily Active User
    private static final String PREFIX_DAU = "dau";
    // 用于缓存帖子热度分数有改变的帖子
    private static final String PREFIX_POST = "post";
    // 用于找回密码的验证码缓存
    private static final String PREFIX_FORGET = "forget";

    // 某个实体的赞
    // like:entity:entityType:entityId -> set(userId)
    public static String getEntityLikeKey(int entityType, int entityId){
        // like:entity:entityType:entityId
        return PREFIX_ENTITY_LIKE + SPLIT + entityType + SPLIT + entityId;
    }

    // 某个用户的赞
    // like:user:userId -> int
    public static String getUserLikeKey(int userId){
        return PREFIX_USER_LIKE + SPLIT + userId;
    }

    // 某个用户关注了某个实体
    // followee:userId:entityType -> zset(entityId, now)
    public static String getFolloweeKey(int userId, int entityType){
        return PREFIX_FOLLOWEE + SPLIT + userId + SPLIT + entityType;
    }

    // 某个实体拥有的粉丝
    // follower:entityType:entityId -> zset(userId, now)
    public static String getFollowerKey(int entityType, int entityId){
        return PREFIX_FOLLOWER + SPLIT + entityType + SPLIT + entityId;
    }

    /**
     * 获取登录验证码
     * @param owner 随机字符串，用于临时标记用户，用户访问登录页面时，给 cookie 临时发一个凭证
     * @return 验证码的 Redis key
     */
    public static String getKaptchaKey(String owner){
        return PREFIX_KAPTCHA + SPLIT + owner;
    }

    // 登录凭证
    // ticket:uuid --> userId
    public static String getTicketKey(String ticket){
        return PREFIX_TICKET + SPLIT + ticket;
    }

    // 缓存用户
    // user:userId --> user
    public static String getUserKey(int userId){
        return PREFIX_TICKET + SPLIT + userId;
    }

    // 单日 UV
    //
    public static String getUVKey(String Date){
        return PREFIX_UV + SPLIT + Date;
    }

    // 区间 UV
    // uv:2022.04.13:2022.05.13
    public static  String getUVKey(String startDate, String endDate){
        return  PREFIX_UV + SPLIT + startDate + SPLIT + endDate;
    }

    // 单日 DAU
    // dau:2022.05.13
    public static String getDAUKey(String Date){
        return PREFIX_DAU + SPLIT + Date;
    }

    // 区间活跃用户
    // dau:2022.04.13:2022.05.13
    public static  String getDAUKey(String startDate, String endDate){
        return  PREFIX_DAU + SPLIT + startDate + SPLIT + endDate;
    }

    // 获得帖子分数的帖子
    public static String getPostScoreKey(){
        return PREFIX_POST + SPLIT + "score";
    }

    // 找回密码验证码
    // forget:email kaptcha
    public static String getForgetKaptcha(String email){
        return PREFIX_FORGET + SPLIT + email;
    }
}
