package com.sean.community.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    private int id;
    private int fromId;             // 发送者
    private int toId;               // 接收者
    private String conversationId;  // 会话 id
    private String content;         // 0 未读，1 已读，2 已删除
    private int status;
    private Date createTime;
}
