package com.sean.community.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * 事件实体：用于 Kafka 消息队列
 * 发送系统消息
 * 点赞，评论，关注等消息
 */
public class Event {
    private String topic;
    private int userId;         // 触发事件的人， 生产者
    private int entityType;     // 实体类型
    private int entityId;       // 实体 id
    private int entityUserId;   // 实体用户 id（例如：帖子的作者）
    private Map<String, Object> data = new HashMap<>();     // 用于以后拓展的属性

    public String getTopic() {
        return topic;
    }

    // 为了使外界可以连续 set 故返回 Event
    public Event setTopic(String topic) {
        this.topic = topic;
        return this;
    }

    public int getUserId() {
        return userId;
    }

    public Event setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public int getEntityType() {
        return entityType;
    }

    public Event setEntityType(int entityType) {
        this.entityType = entityType;
        return this;
    }

    public int getEntityId() {
        return entityId;
    }

    public Event setEntityId(int entityId) {
        this.entityId = entityId;
        return this;
    }

    public int getEntityUserId() {
        return entityUserId;
    }

    public Event setEntityUserId(int entityUserId) {
        this.entityUserId = entityUserId;
        return this;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public Event setData(String key, Object value) {
        this.data.put(key, value);
        return this;
    }
}
