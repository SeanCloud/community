package com.sean.community.entity;

/**
 * 封装分页相关信息
 */

public class Page {
    private int current = 1;    // 当前页码
    private int limit = 10;     // 每页显示上限
    private int rows;           // 数据总数，用于计算总页数
    private String path;           // 查询路径，复用分页连接

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        if(current >= 1){
            this.current = current;
        }
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        if(limit >= 1 && limit <= 100){
            this.limit = limit;
        }

    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        if(rows >= 0){
            this.rows = rows;
        }
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    /**
     * 获取当前页的起始行
     * @return: 当前页的起始行
     */
    public int getOffset(){
        // 1 0
        // 2 10
        // 3 20
        // (current - 1) * limit
        return (current - 1) * limit;
    }

    /**
     * 获取总的页数
     * @return: 总的页数
     */
    public int getTotal(){
        if(rows % limit == 0){
            return rows / limit;
        }else{
            return rows / limit + 1;
        }
    }

    /**
     * 获取起始页, 页码只显示到当前页的前两页
     * @return: 起始页码
     */
    public int getFrom(){
        int from = current - 2;
        return Math.max(from, 1);
    }

    /**
     * 获取结束页码，页面只显示到当前页的后两页
     * @return: 结束页面
     */
    public int getTo(){
        int to = current + 2;
        int total = getTotal();
        return Math.min(to, total);
    }
}
