$(function(){
    $("#getForgetKaptcha").click(getKaptcha);
});

function getKaptcha() {
    // 发送 AJAX 请求之前，将 CSRF 令牌设置到请求消息头中
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });

    // 获取 email
    var email = $('#your-email').val();

    // 发送异步请求
    $.post(
        CONTEXT_PATH + "/forgetKaptcha",
        {"email":email},
        function (data) {
            data = $.parseJSON(data);
            $("#hintBody").text(data.msg); // 这里不能使用 $("#hintModal").text(data.msg);
            $("#hintModal").modal("show");
            setTimeout(function(){
                $("#hintModal").modal("hide");
            }, 3000);
        }
    );
}