$(function(){
    $("#updatePassword").submit(check_data);
    $("form#updatePassword input").focus(clear_error);      // 选择修改密码表单中的所有输入框
    $("#updateHeaderForm").submit(uploadHeader);
});

function check_data() {
    var pwd1 = $("#new-password").val();
    var pwd2 = $("#confirm-password").val();
    if(pwd1 !== pwd2) {
        $("#confirm-password").addClass("is-invalid");
        return false;
    }
    return true;
}

function clear_error() {
    $(this).removeClass("is-invalid");
}

function uploadHeader() {
    // 发送 AJAX 请求之前，将 CSRF 令牌设置到请求消息头中
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
    $.ajax({
        url: "http://up-z2.qiniup.com",
        method: "post",
        processData: false, // 不将表单内容转为字符串
        contentType: false, // 不允许 jquery 设置上传类型， 由浏览器设置
        data: new FormData($("#updateHeaderForm")[0]), // jquery 选择器得到的是 dom 对象数组
        success: function (data) {
            if(data && data.code === 200){
                // 发送 AJAX 请求之前，将 CSRF 令牌设置到请求消息头中
                var token = $("meta[name='_csrf']").attr("content");
                var header = $("meta[name='_csrf_header']").attr("content");
                $(document).ajaxSend(function (e, xhr, options) {
                    xhr.setRequestHeader(header, token);
                });
                // 更新头像访问路径
                $.post(
                    CONTEXT_PATH + "/user/header/url",
                    {"fileName": $("input[name = 'key']").val()},
                    function (data) {
                        data = $.parseJSON(data);
                        if(data.code === 200){
                            window.location.reload();
                        }else{
                            alert(data.msg);
                        }
                    }
                );
            }else{
                alert("上传失败")
            }
        }
    });
    return false;
}