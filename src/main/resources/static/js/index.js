$(function(){
	$("#publishBtn").click(publish);
});

function publish() {
	$("#publishModal").modal("hide");

	// 发送 AJAX 请求之前，将 CSRF 令牌设置到请求消息头中
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function (e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});

	// 获取标题和内容
	var title = $('#recipient-name').val();
	var content = $('#message-text').val();
	// 发送异步请求
	$.post(
		CONTEXT_PATH + "/discussPost/add",
		{"title":title,"content":content},
		function (data) {
			data = $.parseJSON(data);
			$("#hintBody").text(data.msg); // 这里不能使用 $("#hintModal").text(data.msg);
			$("#hintModal").modal("show");
			setTimeout(function(){
				$("#hintModal").modal("hide");
				if(data.code === 200){
					window.location.reload();
				}
			}, 2000);
		}
	);
}