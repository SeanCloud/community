$(function(){
	$(".follow-btn").click(follow);
});

function follow() {
	var btn = this;

	// 发送 AJAX 请求之前，将 CSRF 令牌设置到请求消息头中
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function (e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});

	if($(btn).hasClass("btn-info")) {
		// 关注TA
		$.post(
			CONTEXT_PATH + "/follow",
			{"entityType":3,"entityId":$(btn).prev().val()},
			function (data) {
				data = $.parseJSON(data);
				if(data.code === 200){
					window.location.reload();
				}else{
					alert(data.msg);
					setTimeout(function(){
						window.location.reload();
					}, 2000);
				}
			}
		);
	} else {
		// 取消关注
		$.post(
			CONTEXT_PATH + "/unfollow",
			{"entityType":3,"entityId":$(btn).prev().val()},
			function (data) {
				data = $.parseJSON(data);
				if(data.code === 200){
					window.location.reload();
				}else{
					alert(data.msg);
					setTimeout(function(){
						window.location.reload();
					}, 2000);
				}
			}
		);
		// $(btn).text(" + 关注").removeClass("btn-secondary").addClass("btn-info");
	}
}